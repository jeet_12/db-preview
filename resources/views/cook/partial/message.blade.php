@if(Session::has('message'))
<div id="page-alert">
    <div class="alert-wrap {{ Session::get('alert-class') == 'alert-success' ? 'in' : '' }}">
        <div class="alert alert-success" role="alert">
            <button class="close" data-dismiss="alert" type="button">
                <i class="fa fa-times-circle"></i>
            </button>
            <div class="media">
                <div class="media-left">
                    <span class="icon-wrap icon-wrap-xs icon-circle alert-icon">
                        <i class="fa fa-thumbs-up fa-lg"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="alert-title">{{ Session::get('message') }}</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="alert-wrap {{ Session::get('alert-class') == 'alert-danger' ? 'in' : '' }}">
        <div class="alert alert-danger" role="alert">
            <button class="close" data-dismiss="alert" type="button">
                <i class="fa fa-times-circle"></i>
            </button>
            <div class="media">
                <div class="media-left">
                    <span class="icon-wrap icon-wrap-xs icon-circle alert-icon">
                        <i class="fa fa-times fa-lg"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="alert-title">{{ Session::get('message') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if($errors)
<div class="alert-wrap alert-danger">
    <div class="alert alert-danger" role="alert">
        <button class="close" data-dismiss="alert" type="button">
            <i class="fa fa-times-circle"></i>
        </button>
        <div class="media">
            <div class="media-left">
                    <span class="icon-wrap icon-wrap-xs icon-circle alert-icon">
                        <i class="fa fa-times fa-lg"></i>
                    </span>
            </div>
            <div class="media-body">
                @foreach($errors->all() as $error)
                    {{--<li></li>--}}
                    <h4 class="alert-title">{{$error}}</h4>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="_url" content="{{ URL('') }}"/>

<title>{{ config('app.name', 'Laravel') }}</title>

@include('cook.partial.styles')
@yield('style')
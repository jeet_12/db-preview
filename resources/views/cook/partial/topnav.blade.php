<header id="navbar">
	<div id="navbar-container" class="boxed">
		<div class="navbar-header">
			<a href="{{ url('/') }}" class="navbar-brand">
				<img src="{{ asset('assets/img/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="brand-icon">
				<div class="brand-title">
					<span class="brand-text">{{ config('app.name', 'Laravel') }}</span>
				</div>
			</a>
		</div>
		<div class="navbar-content clearfix">

			<ul class="nav navbar-top-links pull-right">
				<li id="dropdown-user" class="dropdown">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
						<span class="pull-right">
							<img class="img-circle img-user media-object" src="{{ asset('assets/img/av1.png') }}" alt="Profile Picture">
						</span>
						<div class="username hidden-xs">{{ Auth::user()->name }}</div>
					</a>
					<div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">
						<ul class="head-list">
							<br>
							<li>
								<a href="#">
									<i class="fa fa-user fa-fw fa-lg"></i> Profile
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-gear fa-fw fa-lg"></i> Settings
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-lock fa-fw fa-lg"></i> Lock screen
								</a>
							</li>
						</ul>
						<div class="pad-all text-right">
							<a class="btn btn-primary" href="{{ route('logout') }}"
							   onclick="event.preventDefault();
											 document.getElementById('logout-form').submit();">
								<i class="fa fa-sign-out fa-fw"></i> Logout
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</header>

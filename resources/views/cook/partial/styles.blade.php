    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ] -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    {{--<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">--}}

    <!--Nifty Stylesheet [ REQUIRED ]-->
    {{--<link href="{{ asset('assets/css/nifty.min.css') }}" rel="stylesheet">--}}

    <!--Font Awesome [ OPTIONAL ]-->
    {{--<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">--}}

    {{--<!--Switchery [ OPTIONAL ]-->--}}
    {{--<link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet">--}}

    {{--<!--Bootstrap Select [ OPTIONAL ]-->--}}
    {{--<link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">--}}

    {{--<!--Demo [ DEMONSTRATION ]-->--}}
    <link href="{{ asset('assets/css/demo/nifty-demo.min.css') }}" rel="stylesheet">

    <!--Page Load Progress Bar [ OPTIONAL ]-->
    {{--<link href="{{ asset('assets/plugins/pace/pace.min.css') }}" rel="stylesheet">--}}
{{----}}
    {{--<script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>--}}
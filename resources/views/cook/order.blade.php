<?php
$page_name = $name;
?>
@extends('cook.layout.master')
@section('style')
    <link href="{{ asset('assets/grid/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/grid/cook/style.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div id="page-content" style="padding-left: 22px;padding-right: 22px;">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h1 class="panel-title">{{ ucwords(str_replace("-"," ",$page_name)) }} Detail</h1>
                    </div>
                    <div class="panel-body">
                        <div class="grid-view" >
                            <div class="controls" data-ref="controls" style="background: #f2f2f2 !important;">
                                {{--<button type="button" class="control control-filter" data-ref="filter" data-color="all">All</button>--}}
                                {{--<button type="button" class="control control-filter" data-ref="filter" data-color="green">Green</button>--}}
                                {{--<button type="button" class="control control-filter" data-ref="filter" data-color="blue">Blue</button>--}}
                                {{--<button type="button" class="control control-filter" data-ref="filter" data-color="pink">Pink</button>--}}
                                {{--<button type="button" class="control control-filter" data-ref="filter" data-color="none">None</button>--}}
                                {{--<button type="button" class="control control-sort" data-ref="sort" data-key="publishedDate" data-order="asc">Asc</button>--}}
                                {{--<button type="button" class="control control-sort" data-ref="sort" data-key="publishedDate" data-order="desc">Desc</button>--}}
                            </div>

                            <div class="container-grid" data-ref="container-grid">
                                <div class="gap" data-ref="first-gap"></div>
                                <div class="gap"></div>
                                <div class="gap"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pageScript')
    <script src="{{ asset('assets/grid/mock-api.js') }}"></script>
    <script src="{{ asset('assets/grid/mixitup.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script>
        var items_ajax = "";
        var _token              =  token;
        var _url                =  baseUrl;
        var url                 = _url+'/cook/order/table/ajax';
        var form_data           = { _token:_token };

        $.ajax({
            url: url,
            type: 'GET',
            data: form_data,
            dataType:"json",
            success: function(data)
            {

                items_ajax =data.table;
                incTimer();
            },
            async: false
        });

        var api = new Api(items_ajax);


        var controls  = document.querySelector('[data-ref="controls"]');
//        var filters   = document.querySelectorAll('[data-ref="filter"]');
//        var sorts     = document.querySelectorAll('[data-ref="sort"]');
        var container = document.querySelector('[data-ref="container-grid"]');


        var firstGap = document.querySelector('[data-ref="first-gap"]');

        // We'll need to keep track of our active current filter so
        // that we can sort within the current filter.

        var activeColor = '';

        // Instantiate and configure the mixer

        var mixer = mixitup(container, {
            selectors: {
                target: '[data-ref="item"]' // Query targets with an attribute selector to keep our JS and styling classes seperate
            },
            layout: {
                siblingAfter: firstGap // Ensure the first "gap" element is known to mixitup incase of insertion into an empty container
            },
            data: {
                uidKey: 'id' // Our data model must have a unique id. In this case, its key is 'id'
            },
            render: { // We must provide a target render function incase we need to render new items not in the initial dataset (not used in this demo)
                target: function(item) {
                    return '<div class="item ' + item.color + '" data-ref="item">' +
                        '<p>Table No : ' +item.id + '<br>' +
                        'Customer : ' + item.cust_name + '<br>' +
                        'Time : <span id="timer"></span></p>' +
                        '</div>';
                }
            }
        });

        /**
         * A helper function to set an active styling class on an active button,
         * and remove it from its siblings at the same time.
         *
         * @param {HTMLElement} activeButton
         * @param {HTMLELement[]} siblings
         * @return {void}
         */

//        function activateButton(activeButton, siblings) {
//            var button;
//            var i;
//
//            for (i = 0; i < siblings.length; i++) {
//                button = siblings[i];
//
//                button.classList[button === activeButton ? 'add' : 'remove']('control-active');
//            }
//        }

        /**
         * A click handler to detect the type of button clicked, read off the
         * relevent attributes, call the API, and trigger a dataset operation.
         *
         * @param   {HTMLElement} button
         * @return  {void}
         */

//        function handleButtonClick(button) {
//            // Define default values for color, sortBy and order
//            // incase they are not present in the clicked button
//
//            var color  = activeColor;
//            var sortBy = 'id';
//            var order  = 'asc';
//
//            // If button is already active, or an operation is in progress, ignore the click
//
//            if (button.classList.contains('control-active') || mixer.isMixing()) return;
//
//            // Else, check what type of button it is, if any
//
//            if (button.matches('[data-ref="filter"]')) {
//                // Filter button
//
//                activateButton(button, filters);
//
//                color = activeColor = button.getAttribute('data-color');
//            } else if (button.matches('[data-ref="sort"]')) {
//                // Sort button
//
//                activateButton(button, sorts);
//
//                sortBy = button.getAttribute('data-key');
//                order = button.getAttribute('data-order');
//            } else {
//                // Not a button
//
//                return;
//            }
//
//            // Now that we have our color filter and sorting order, we can fetch some data from the API.
//
//            api.get({
//                color: color,
//                $sort_by: sortBy,
//                $order: order
//            })
//                .then(function(items_ajax) {
//                    // Our api returns an array of items which we can send
//                    // straight to our mixer using the .dataset() method
//
//                    return mixer.dataset(items_ajax);
//                })
//                .then(function(state) {
//                    console.log('fetched ' + state.activeDataset.length + ' items');
//                })
//                .catch(console.error.bind(console));
//        }

//        // We can now set up a handler to listen for "click" events on our UI buttons
//
//        controls.addEventListener('click', function(e) {
//            handleButtonClick(e.target);
//        });

        // Set controls the active controls on startup to match the default filter and sort

//        activateButton(controls.querySelector('[data-color="all"]'), filters);
//        activateButton(controls.querySelector('[data-order="asc"]'), sorts);

        // Finally, load the full dataset into the mixer

        mixer.dataset(items_ajax)
            .then(function(state) {
                console.log('loaded ' + state.activeDataset.length + ' items');
            });

        function incTimer() {

            var currentMinutes = Math.floor(totalSecs / 60);
            var currentSeconds = totalSecs % 60;
            if(currentSeconds <= 9) currentSeconds = "0" + currentSeconds;
            if(currentMinutes <= 9) currentMinutes = "0" + currentMinutes;
            totalSecs++;
            $("#timer").text(currentMinutes + ":" + currentSeconds);
            setTimeout('incTimer()', 1000);
        }
        var totalSecs = 0;

    </script>
@endsection
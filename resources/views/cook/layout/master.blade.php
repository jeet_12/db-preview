<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('cook.partial.head')
    @yield('style')
</head>    
<body>
    <div id="container" class="effect mainnav-lg">
{{--        @include('cook.partial.topnav')--}}

        <div class="boxed">

            <div id="content-container">
{{--                @include('cook.partial.message')--}}
                @yield('content')
            </div>
        </div>

            {{--@include('cook.partial.footer')--}}
    </div> 
    @include('cook.partial.scripts')
    @yield("pageScript")
</body>
</html>

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('partial.head')
    @yield('style')
</head>    
<body>
    <div id="container" class="effect mainnav-lg">
        @include('partial.topnav')

        <div class="boxed">

            <div id="content-container">
                @include('partial.message')
                @yield('content')
            </div>

            @include('partial.sidebar')
        </div>

            @include('partial.footer')
    </div> 
    @include('partial.scripts')
    @yield("pageScript")
</body>
</html>

@extends('admin.layouts.master')

@section('content')
<!--Page Title-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div id="page-title">
    <h1 class="page-header text-overflow">Dashboard</h1>

    <!--Searchbox-->
    <div class="searchbox">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search..">
            <span class="input-group-btn">
                <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End page title-->

<!--Page content-->
<!--===================================================-->
<div id="page-content">
    <!--Tiles - Bright Version-->
    <!--===================================================-->
    <div class="row">
        <div class="col-sm-6 col-lg-3">

            <!--Registered User-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div class="panel media pad-all">
                <div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-success">
									<i class="fa fa-user fa-2x"></i>
									</span>
                </div>

                <div class="media-body">
                    <p class="text-2x mar-no text-thin">241</p>
                    <p class="text-muted mar-no">Registered User</p>
                </div>
            </div>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

        </div>
        <div class="col-sm-6 col-lg-3">

            <!--New Order-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div class="panel media pad-all">
                <div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-info">
									<i class="fa fa-shopping-cart fa-2x"></i>
									</span>
                </div>

                <div class="media-body">
                    <p class="text-2x mar-no text-thin">543</p>
                    <p class="text-muted mar-no">New Order</p>
                </div>
            </div>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

        </div>
        <div class="col-sm-6 col-lg-3">

            <!--Comments-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div class="panel media pad-all">
                <div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-warning">
									<i class="fa fa-comment fa-2x"></i>
									</span>
                </div>

                <div class="media-body">
                    <p class="text-2x mar-no text-thin">34</p>
                    <p class="text-muted mar-no">Comments</p>
                </div>
            </div>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

        </div>
        <div class="col-sm-6 col-lg-3">

            <!--Sales-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div class="panel media pad-all">
                <div class="media-left">
									<span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
									<i class="fa fa-dollar fa-2x"></i>
									</span>
                </div>

                <div class="media-body">
                    <p class="text-2x mar-no text-thin">654</p>
                    <p class="text-muted mar-no">Sales</p>
                </div>
            </div>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

        </div>
    </div>
    <!--===================================================-->
    <!--End Tiles - Bright Version-->
</div>
<!--===================================================-->
<!--End page content-->
@endsection

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('admin.partial.head')
    @yield('style')
</head>    
<body>
    <div id="container" class="effect mainnav-lg">
        @include('admin.partial.topnav')

        <div class="boxed">

            <div id="content-container">
                @include('admin.partial.message')
                @yield('content')
            </div>

            @include('admin.partial.sidebar')    
        </div>

            @include('admin.partial.footer')
    </div> 
    @include('admin.partial.scripts')
    @yield("pageScript")
</body>
</html>

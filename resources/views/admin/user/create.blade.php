<?php
$page_name = $name;
?>
@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Add {{ ucwords(str_replace("-"," ",$page_name)) }}</h3>
                    </div>
                    {{ Form::open(['route' => 'user.store','class' => 'form-horizontal']) }}
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-lg-3 control-label">Username</label>
                                    <div class="col-lg-7">
                                        {{Form :: text('name',null ,['class'=> "form-control",'placeholder' => "User Name"])}}

                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="demo-textarea-input">Phone No.</label>
                                    <div class="col-md-7">
                                        {{Form :: text('phone',null ,['class'=> "form-control",'placeholder' => "Phone No."])}}

                                    @if ($errors->has('phone'))
                                        <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                                    @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Gender</label>
                                    <div class="col-md-7">
											{!! Form::select('gender',['male'=>'Male','female'=>'Female'],null, ['class'=> "selectpicker", 'data-live-search'=>"true", 'data-width'=>"100%"]) !!}
                                    @if ($errors->has('gender'))
                                        <span class="help-block"><strong>{{ $errors->first('gender') }}</strong></span>
                                    @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label" >Email</label>
                                    <div class="col-md-7">
                                        {{ Form :: email('email',null ,['class'=> "form-control",'placeholder' => "Email"])}}

                                        @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="col-lg-3 control-label">Password</label>
                                    <div class="col-lg-7">
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Role</label>
                                    <div class="col-md-7">
                                        {!! Form::select('role',$role,null, ['class'=> "selectpicker", 'data-live-search'=>"true", 'data-width'=>"100%"]) !!}
                                        @if ($errors->has('role'))
                                            <span class="help-block"><strong>{{ $errors->first('role') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">Status</label>
                                    <div class="col-md-7">
                                        {!! Form::select('status',$status,null, ['class'=> "selectpicker", 'data-live-search'=>"true", 'data-width'=>"100%"]) !!}
                                        @if ($errors->has('status'))
                                            <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-7 col-sm-offset-3">
                                        <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> &nbsp;Submit</button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

<?php
$page_name = $name;
?>
@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ ucwords(str_replace("-"," ",$page_name)) }} Detail</h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Username</label>
                                <div class="col-lg-7">
                                    {{Form :: label('name',$user->name ,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="demo-textarea-input">Email</label>
                                <div class="col-md-7">
                                    {{Form :: label('email',$user->email,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Contact No.</label>
                                <div class="col-lg-7">
                                    {{Form :: label('phone',$user->phone,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Gender</label>
                                <div class="col-lg-7">
                                    {{Form :: label('gender',$user->gender,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Role</label>
                                <div class="col-lg-7">
                                    @foreach($user->roles as $role)
                                        {{ Form :: label('role',ucwords($role['display_name']),['class'=> "form-control"]) }}
                                        @if(count($user->roles) > 1)
                                            {{ "," }}
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-3">
                                    <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Back</a>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

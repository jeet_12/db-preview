<?php
$page_name = $name;
?>
@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit {{ ucwords(str_replace("-"," ",$page_name)) }}</h3>
                    </div>
                    {{ Form::model($product,['route' => ['product-categories.update',$product->id],'method' => 'PUT','class' => 'form-horizontal','enctype'=>"multipart/form-data"]) }}
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Product Name</label>
                                <div class="col-lg-7">
                                    {{Form :: text('name',null ,['class'=> "form-control",'placeholder' => "Product Name"])}}

                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="demo-textarea-input">Product Description</label>
                                <div class="col-md-7">
                                    {{Form :: textarea('desc',null ,['class'=> "form-control",'id'=>"", 'rows'=>'3','placeholder' => "Product Description"])}}

                                    @if ($errors->has('desc'))
                                        <span class="help-block"><strong>{{ $errors->first('desc') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Product Price</label>
                                <div class="col-lg-7">
                                    {{Form :: text('price',null ,['class'=> "form-control",'placeholder' => "Product Price"])}}

                                    @if ($errors->has('price'))
                                        <span class="help-block"><strong>{{ $errors->first('price') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Product Type</label>
                                <div class="col-lg-7">
                                    {{Form :: select('type',$type ,$product->type_id,['class'=> "selectpicker", 'data-live-search'=>"true", 'data-width'=>"100%"])}}

                                    @if ($errors->has('type'))
                                        <span class="help-block"><strong>{{ $errors->first('type') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Product Categories</label>
                                <div class="col-lg-7">
                                    {{Form :: select('categories',$categories ,$product->categories_id,['class'=> "selectpicker", 'data-live-search'=>"true", 'data-width'=>"100%"])}}

                                    @if ($errors->has('categories'))
                                        <span class="help-block"><strong>{{ $errors->first('categories') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">Product Image </label>
                                <div class="col-md-7">
                                    {!! Html::image( $product->image_id != 0 ? '/assets/img/'.$name.'/'.$product->image->name.'.'.$product->image->type : '', 'Product image', array('class' => 'img-lg form-control')) !!}
                                    <br>
                                    {!! Form::file('image', ['class'=>'pull-left btn btn-default btn-file']) !!}

                                    @if ($errors->has('image'))
                                        <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-3">
                                    <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                    <button type="submit" class="btn btn-primary "><i class="fa fa-send"></i> &nbsp;Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection


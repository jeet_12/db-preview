<?php
$page_name = $name;
$allRecord = $products;
$primary_key = 'id';

$primary_column = 0;
$primary_table_row = $page_name.'_table_row';
$primary_table_col_del = 'table-col-del';
$singleRecord = "";
?>

@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ ucwords(str_replace("-"," ",$page_name)) }} List</h3>
                    </div>

                    <div class="panel-body">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left">
                                    <a href="{{ URL('admin/'.$page_name.'/create') }}" id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">Add</a>
                                </div>
                                <div class="col-sm-6 table-toolbar-right">
                                    {!! Form::open(array('url'=>'/admin/'.$page_name.'-search')) !!}
                                        <div class="form-group">
                                            {{ Form::text('search', null, array('class' => 'form-control','id' => '' , 'autocomplete' =>"off" , 'placeholder' => 'Search')) }}
                                        </div>
                                        <div class="btn-group">
                                            <div class="btn-group">
                                                <button class="btn btn-primary">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width:4ex">ID</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $a = $allRecord->firstItem(); ?>
                                @if(count($allRecord) > 0)
                                    @foreach($allRecord as $singleRecord)
                                        <?php  $primary_column = $singleRecord->$primary_key;  ?>
                                <tr class="{{ $primary_table_row.$primary_column }}" >
                                    <td><a href="#" class="btn-link">{{ $a }}</a></td>
                                    <td>{{ $singleRecord->name }}</td>
                                    <td>{{ $singleRecord->type->name }}</td>
                                    <td>{{ $singleRecord->categories->name }}</td>
                                    <td>{{ $singleRecord->price }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-xs btn-default add-tooltip" data-toggle="tooltip" href="{{ url('admin/'.$page_name.'/'.$primary_column) }}" data-original-title="View" data-container="body"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-xs btn-default add-tooltip" data-toggle="tooltip" href="{{ url('admin/'.$page_name.'/'.$primary_column.'/edit') }}" data-original-title="Edit" data-container="body"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger add-tooltip {{ $primary_table_col_del }}" data-id="{{ $primary_column }}" data-pid="{{ $page_name }}" title="Delete" data-container="body"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                        <?php $a++; ?>
                                    @endforeach
                                @else
                                    <tr><td class="text-center" colspan="4">No Records !</td></tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="text-left">
                                <div class="">Showing {{ $allRecord->firstItem() }} to {{ $allRecord->lastItem() }} of {{ $allRecord->total() }} entries</div>
                            </div>
                            <div class="text-right">
                                <div class="pagination mar-no">{{ $allRecord->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

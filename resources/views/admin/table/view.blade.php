<?php
$page_name = $name;
?>
@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ ucwords(str_replace("-"," ",$page_name)) }} Detail</h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Table Name</label>
                                <div class="col-lg-7">
                                    {{Form :: label('no',$table->no ,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Table Name</label>
                                <div class="col-lg-7">
                                    {{Form :: label('name',$table->name ,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="demo-textarea-input">Table Description</label>
                                <div class="col-md-7">
                                    {{Form :: label('desc',$table->desc,['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Table Status</label>
                                <div class="col-lg-7">
                                    {{Form :: label('status',ucwords($table->status),['class'=> "form-control"])}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-3">
                                    <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Back</a>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

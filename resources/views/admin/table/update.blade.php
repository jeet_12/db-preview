<?php
$page_name = $name;
?>
@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit {{ ucwords(str_replace("-"," ",$page_name)) }}</h3>
                    </div>
                    {{ Form::model($table,['route' => ['table.update',$table->id],'method' => 'PUT','class' => 'form-horizontal']) }}
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group{{ $errors->has('no') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Table No.</label>
                                <div class="col-lg-7">
                                    {{Form :: text('no',null ,['class'=> "form-control",'placeholder' => "Table No."])}}

                                    @if ($errors->has('no'))
                                        <span class="help-block"><strong>{{ $errors->first('no') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Table Name</label>
                                <div class="col-lg-7">
                                    {{Form :: text('name',null ,['class'=> "form-control",'placeholder' => "Table Name"])}}

                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="demo-textarea-input">Table Description</label>
                                <div class="col-md-7">
                                    {{Form :: textarea('desc',null ,['class'=> "form-control",'id'=>"", 'rows'=>'3','placeholder' => "Table Description"])}}

                                    @if ($errors->has('desc'))
                                        <span class="help-block"><strong>{{ $errors->first('desc') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Table Status</label>
                                <div class="col-lg-7">
                                    {{Form :: Select('status',$status ,$table->status,['class'=> "selectpicker", 'data-live-search'=>"true", 'data-width'=>"100%"])}}

                                    @if ($errors->has('status'))
                                        <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-3">
                                    <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> &nbsp;Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection


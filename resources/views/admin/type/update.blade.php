<?php
$page_name = $name;
?>
@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit {{ ucwords(str_replace("-"," ",$page_name)) }}</h3>
                    </div>
                    {{ Form::model($types,['route' => ['product-type.update',$types->id],'method' => 'PUT','class' => 'form-horizontal','enctype'=>"multipart/form-data"]) }}
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label">Product Type Name</label>
                                <div class="col-lg-7">
                                    {{Form :: text('name',null ,['class'=> "form-control",'placeholder' => "Product Type Name"])}}

                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="demo-textarea-input">Product Type Description</label>
                                <div class="col-md-7">
                                    {{Form :: textarea('desc',null ,['class'=> "form-control",'id'=>"", 'rows'=>'3','placeholder' => "Product Type Description"])}}

                                    @if ($errors->has('desc'))
                                        <span class="help-block"><strong>{{ $errors->first('desc') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">Product Type Image</label>
                                <div class="col-md-7">
                                    {!! Html::image( $types->image_id != 0 ? '/assets/img/'.$name.'/'.$types->image->name.'.'.$types->image->type : '', 'Product Type image', array('class' => 'img-lg form-control')) !!}
                                    <br>
                                    {!! Form::file('image', ['class'=>'pull-left btn btn-default btn-file']) !!}

                                    @if ($errors->has('image'))
                                        <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-7 col-sm-offset-3">
                                    <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                    <button type="submit" class="btn btn-primary "><i class="fa fa-send"></i> &nbsp;Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection


<!--MAIN NAVIGATION-->
			<!--===================================================-->
			<nav id="mainnav-container">
				<div id="mainnav">
					<!--Shortcut buttons-->
					<!--================================-->
					<div id="mainnav-shortcut">
						
					</div>
					<!--================================-->
					<!--End shortcut buttons-->

					<!--Menu-->
					<!--================================-->
					<div id="mainnav-menu-wrap">
						<div class="nano">
							<div class="nano-content">
								<ul id="mainnav-menu" class="list-group">
						
									<!--Category name-->
									<li class="list-header">Navigation</li>
						
									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'dashboard') !== false)"active-link"@endif >
										<a href="{{ url('/') }}">
											<i class="fa fa-dashboard"></i>
											<span class="menu-title">
												<strong>Dashboard</strong>
											</span>
										</a>
									</li>
						
									{{--<!--Menu list item-->--}}
									<li class=@if(strpos(url()->current(), '-grid') !== false)"active-link active"@endif>
										<a href="#">
											<i class="fa fa-edit"></i>
											<span class="menu-title">
												<strong>Master</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/table-grid') }}">Table Grid</a></li>
											<li><a href="{{ url('admin/order-grid') }}">Order Grid</a></li>
											{{--<li><a href="{{ url('admin/customer') }}">Customer</a></li>--}}
											{{--<li><a href="{{ url('admin/recipe') }}">Recipe</a></li>--}}
										</ul>
									</li>

									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'product') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-plus-square"></i>
											<span class="menu-title">
												<strong>Product</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/product') }}">Product List</a></li>
											<li><a href="{{ url('admin/product-type') }}">Product Type</a></li>
											<li><a href="{{ url('admin/product-categories') }}">Product Category</a></li>
										</ul>
									</li>
									<li class=@if(strpos(url()->current(), 'table') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-table"></i>
											<span class="menu-title">
												<strong>Table</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/table') }}">Table List</a></li>
											<li><a href="{{ url('admin/table/create') }}">Add Table</a></li>
										</ul>
									</li>
									<li class=@if(strpos(url()->current(), 'order') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-bars"></i>
											<span class="menu-title">
												<strong>Order</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/order') }}">Order List</a></li>
											{{--<li><a href="{{ url('admin/order') }}">Add Table</a></li>--}}
										</ul>
									</li>

									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'recipe') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-file"></i>
											<span class="menu-title">
												<strong>Recipe</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/recipe-list') }}">Recipe List</a></li>
											<li><a href="{{ url('admin/recipe-add') }}">Add Recipe</a></li>
										</ul>
									</li>
									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'user') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-plug"></i>
											<span class="menu-title">
												<strong>User</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/user') }}">User List</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!--================================-->
					<!--End menu-->

				</div>
			</nav>
			<!--===================================================-->
			<!--END MAIN NAVIGATION-->
<?php
//        dd($order->order_detail);
$page_name = $name;
$allRecord = $order->order_detail;
$primary_key = 'id';
$total=0;
$primary_column = 0;
$primary_table_row = $page_name.'_table_row';
$primary_table_col_del = 'table-col-del';
$singleRecord = "";
?>

@extends('admin.layouts.master')
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ ucwords(str_replace("-"," ",$page_name)) }} Detail</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row col-lg-12">
                            <div class="col-lg-4">
                                <h4><b>Order No. {{ $order->no }}</b></h4>
                                <b>Order Date: <span class="text-muted">{{ $order->date." ".$order->time }}</span></b><br>
                                <b>Table : {{ $order->table->name }}</b><br>
                                <b>Status:</b> <span class="label {{ $order->status == 'complete' ? "label-success" : "label-info" }}">{{ $order->status }}</span>
                            </div>

                            <div class="col-lg-4">
                                <h4><b>From</b></h4>
                                <address>
                                    <strong><i class="fa fa-user-o" aria-hidden="true"></i>{{ $order->customer->name }}</strong>
                                    <br><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;{{ $order->customer->phone ? $order->customer->phone : '-' }}
                                    <br><i class="fa fa-envelope-o"></i>&nbsp;{{ $order->customer->email ? $order->customer->email : '-' }}
                                </address>
                            </div>
                            <div class="col-lg-4">
                                <h4><b>Waiter</b></h4>
                                <address>
                                    <strong><i class="fa fa-user-o" aria-hidden="true"></i>{{ $order->user->name }}</strong>
                                    <br><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;{{ $order->user->phone ? $order->user->phone : '-' }}
                                    <br><i class="fa fa-envelope-o"></i>&nbsp;{{ $order->user->email ? $order->user->email : '-' }}
                                </address>
                            </div>
                        </div>

                        <div class="row col-lg-4"></div>
                        <div class="row col-lg-8">
                            <div class="clearfix"></div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th>Product Name</th>
                                            <th>Product Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $a = 1 ?>
                                        @if(count($allRecord) > 0)
                                            @foreach($allRecord as $singleRecord)
                                                <?php  $primary_column = $singleRecord->$primary_key;  ?>
                                                <tr class="{{ $primary_table_row.$primary_column }}" >
                                                    <td><a href="#" class="btn-link">{{ $a }}</a></td>
                                                    <td>{{ $singleRecord->product_name }}</td>
                                                    <td>{{ $singleRecord->product_price }}</td>
                                                </tr>
                                                <?php $a++; $total +=(int)$singleRecord->product_price;  ?>
                                            @endforeach
                                                <tr>
                                                    <td colspan="2" class="text-right"><h4>Total : </h4></td>
                                                    <td><h4>{{ $total }}</h4></td>
                                                </tr>
                                        @else
                                            <tr><td class="text-center" colspan="3">No Records !</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php
$page_name = $name;
$allRecord = 1;
?>

@extends('admin.layouts.master')
@section('style')
    <!--Bootstrap Timepicker [ OPTIONAL ]-->
    <link href="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div id="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ ucwords(str_replace("-"," ",$page_name)) }}</h3>
                    </div>
                    {{ Form::open(['route' => 'order.store','class' => 'form-horizontal']) }}
                    <div class="panel-body">
                        <fieldset>
                            <div class="row col-lg-12">
                                <div class="col-lg-4">
                                    <h4><b>Order No. {{ $order_no  }}{{Form :: hidden("order_no",$order_no,["id"=>"order_no"])}}</b></h4>
                                    <b>Order Date:</b> <span class="text-muted"></span>
                                    <div id="demo-dp-component">
                                        <div class="input-group date">
                                            <input type="text" name="date" value="" class="form-control">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                                        </div>
                                    </div>
                                    <br>
                                    <b>Order Time: </b><div class="input-group date">
                                        <input id="demo-tp-com" name="time" type="text" value="" class="form-control">
                                        <span class="input-group-addon"><i class="fa fa-clock-o fa-lg"></i></span>
                                    </div><br>
                                    <b>Table No. :</b><div class="input-group">
                                        <input type="text" name="no" value="" class="form-control">
                                    </div><br>
                                    <b>Status:</b><div class="input-group">
                                        {{Form :: Select('status',$status ,null,['class'=> "selectpicker", 'data-live-search'=>"true"])}}
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <h4><b>Waiter</b></h4>
                                    <address>
                                        <strong>
                                            <b>Name: </b>
                                            <div class="input-group">
                                                {{Form :: Select('waiter',$user ,null,['class'=> "selectpicker", 'data-live-search'=>"true"])}}
                                            </div>
                                        </strong>
                                    </address>
                                </div>
                                <div class="col-lg-4">
                                    <h4><b>Customer</b></h4>
                                    <address>
                                        <strong>
                                            <b>Name: </b>
                                            <div class="input-group">
                                                {{Form :: Select('customer',$customer ,null,['class'=> "selectpicker", 'data-live-search'=>"true"])}}
                                            </div>
                                        </strong>
                                    </address>
                                </div>

                            </div>

                            <div class="row col-lg-12 text-center"><h3>Order Details</h3></div>
                            <div class="row col-lg-12">
                                <div class="clearfix"></div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped" id="order_details">
                                        <thead>
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                            <th></th>
                                            <th>Quantity</th>
                                            <th></th>
                                            <th>Amount</th>
                                            <th><a class="add-prod pull-right" href="javascript: void(0)" onclick="addRow()" tiitle="Click to add more"><i class="fa fa-plus" aria-hidden="true"></i></a></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $a = 0 ?>
                                            {{Form :: hidden("rowCount",1,["id"=>"rowCount"])}}
                                                <tr class="row-prod" id="table-row-{{ $a }}">
                                                    <td><a href="#" class="btn-link">{{ $a+1 }}</a></td>
                                                    <td>
                                                        {{Form :: Select('product_name[]',$product ,null,['class'=> "form-control",'id'=>"product_name[".$a."]", "onchange"=>"ProductChange(".$a.")" ,'data-live-search'=>"true", 'data-width'=>"100%"])}}
                                                    </td>
                                                    <td>
                                                        {{Form :: text("product_price[]",null,["class" => "form-control","id"=>"price[".$a."]" ,"readonly" ])}}
                                                    </td>
                                                    <td>X</td>
                                                    <td>
                                                        {{Form :: number("product_quantity[]",null,["class" => "form-control","id"=>"qty[".$a."]" ,"onblur"=>"amount(".$a.")","onmouseover"=>"amount(".$a.")"])}}
                                                    </td>
                                                    <td>=</td>
                                                    <td>
                                                        {{Form :: text("amount[]",null,["class" => "form-control" ,"readonly","onblur"=>"amount(".$a.")","id"=>"amount[".$a."]","onmouseover"=>"amount(".$a.")"])}}
                                                    </td>
                                                    <td><a class="remove-prod pull-right" id="removeRow{{$a}}" href="javascript:void(0)" data-id="{{$a}}"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                                </tr>
                                                <?php $a++; ?>
                                            <tr>
                                                <td colspan="6" class="text-right"><h4>Total : </h4></td>
                                                <td><h4><div id="total_amount">{{Form :: text("total",null,["class" => "form-control" ,"readonly","onblur"=>"total()","id"=>"total"])}}</div></h4></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row col-lg-12">
                                <div class="form-group">
                                    <div class="col-sm-7 col-sm-offset-5">
                                        <a href="{{ URL('admin/'.$page_name) }}" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> &nbsp;Submit</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section("pageScript")
    <script>
        var rowCount = parseInt(document.getElementById("rowCount").value);
        function addRow() {
            var a=parseInt(document.getElementById("rowCount").value)+1;
            var table_row =
                '<tr id="table-row-'+a+'">' +
                '<td><a href="#" class="btn-link">'+a+'</a></td>' +
                '<td>' +
                '<select name="product_name[]" id="product_name['+rowCount+']" onchange="ProductChange('+rowCount+')" class="form-control" data-live-search="true" >' +
                '@foreach($products as $prod)' +
                '<option value="{{ $prod->id }}">{{ $prod->name }}</option>' +
                '@endforeach' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<input type="text" name="product_price[]" value="00" class="form-control" id="price['+rowCount+']" readonly >'+
                '</td>' +
                '<td>' +
                'X'+
                '</td>' +
                '<td>' +
                '<input type="number" name="product_quantity[]" value="0" class="form-control" id="qty['+rowCount+']" onblur="amount('+rowCount+')" onmouseover="amount('+rowCount+')" >'+
                '</td>' +
                '<td>' +
                '='+
                '</td>' +
                '<td>' +
                '<input type="text" name="amount[]" value="00" class="form-control" id="amount['+rowCount+']" onblur="amount('+rowCount+')" onmouseover="amount('+rowCount+')" readonly >'+
                '</td>' +
                '<td>'+
                '<a class="remove-prod pull-right" id="removeRow'+rowCount+'" href="javascript:void(0);" data-id="'+rowCount+'" href="javascript: void(0)"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                '</td>'+
                '</tr>';
            $('#order_details tr:last').before(table_row);
            ++rowCount;
        }

        function removeRow(id) {
            var table_row ="table#order_details tr#table-row-"+id;
            $(table_row).remove();
        }

        function amount(no) {
            var price = document.getElementById("price["+no+"]").value;
            var qty = document.getElementById("qty["+no+"]").value;
            var final_amount = price * qty;
            document.getElementById("amount["+no+"]").value = final_amount;
            total();
        }

        function total() {
            var total = 0;
            for (i = 0; i <= rowCount; i++) {
                if(document.getElementById("amount["+i+"]"))
                {
                    total +=parseInt(document.getElementById("amount["+i+"]").value);
                }else{
                    total +=0;
                }
            }

            document.getElementById("total").value = total;
        }
        function ProductChange(no) {
            var id = document.getElementById("product_name["+no+"]").value;
            var _token =  token;
            var _url =  baseUrl;
            var form_data = {_token : _token, id:id};
            var url = _url+'/admin/getprice';

            $.ajax({
                url: url,
                type: 'get',
                data: form_data,
                dataType:"json",
                success: function(result)
                {
                    if(result >= 0)
                    {
                        document.getElementById("price["+no+"]").value = result;
                    }else{
                        document.getElementById("price["+no+"]").value = 0.00;
                    }
                    amount(no);
                }
            });
        }


        $(function() {
            total();
            $( "#addRow" ).click(function() {
                addRow();
            });

            $(document).on("click",".remove-prod",function() {
                var id=$(this).attr("data-id");
                removeRow(id);
            });

        });
    </script>
    <!--Chosen [ OPTIONAL ]-->
    <script src="{{ asset('assets/plugins/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo/form-component.js') }}"></script>
    <!--Bootstrap Timepicker [ OPTIONAL ]-->
    <script src="{{ asset('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
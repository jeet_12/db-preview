<!--MAIN NAVIGATION-->
			<!--===================================================-->
			<nav id="mainnav-container">
				<div id="mainnav">
					<!--Shortcut buttons-->
					<!--================================-->
					<div id="mainnav-shortcut">
						
					</div>
					<!--================================-->
					<!--End shortcut buttons-->

					<!--Menu-->
					<!--================================-->
					<div id="mainnav-menu-wrap">
						<div class="nano">
							<div class="nano-content">
								<ul id="mainnav-menu" class="list-group">
						
									<!--Category name-->
									<li class="list-header">Navigation</li>
						
									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'dashboard') !== false)"active-link"@endif >
										<a href="{{ url('admin/dashboard') }}">
											<i class="fa fa-dashboard"></i>
											<span class="menu-title">
												<strong>Dashboard</strong>
											</span>
										</a>
									</li>
						
									<!--Menu list item-->
									<li>
										<a href="#">
											<i class="fa fa-edit"></i>
											<span class="menu-title">
												<strong>Master</strong>
											</span>
											<i class="arrow"></i>
										</a>
						
										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/table') }}">Table</a></li>
											<li><a href="{{ url('admin/customer') }}">Customer</a></li>
											<li><a href="{{ url('admin/recipe') }}">Recipe</a></li>
										</ul>
									</li>

									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'product') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-plus-square"></i>
											<span class="menu-title">
												<strong>Product</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/product') }}">Product List</a></li>
											<li><a href="{{ url('admin/product-type') }}">Product Type</a></li>
											<li><a href="{{ url('admin/product-category') }}">Product Category</a></li>
										</ul>
									</li>

									<!--Menu list item-->
									<li class=@if(strpos(url()->current(), 'recipe') !== false)"active-link active"@endif >
										<a href="#">
											<i class="fa fa-file"></i>
											<span class="menu-title">
												<strong>Recipe</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/recipe-list') }}">Recipe List</a></li>
											<li><a href="{{ url('admin/recipe-add') }}">Add Recipe</a></li>
										</ul>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-table"></i>
											<span class="menu-title">
												<strong>Table</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/table-list') }}">Table List</a></li>
											<li><a href="{{ url('admin/table-add') }}">Add Table</a></li>
										</ul>
									</li>
									<!--Menu list item-->
									<li>
										<a href="#">
											<i class="fa fa-plug"></i>
											<span class="menu-title">
												<strong>System Settings</strong>
											</span>
											<i class="arrow"></i>
										</a>

										<!--Submenu-->
										<ul class="collapse">
											<li><a href="{{ url('admin/user') }}">Users</a></li>
											<li><a href="{{ url('admin/role') }}">Role</a></li>
											<li><a href="{{ url('admin/Permission') }}">Permission</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!--================================-->
					<!--End menu-->

				</div>
			</nav>
			<!--===================================================-->
			<!--END MAIN NAVIGATION-->
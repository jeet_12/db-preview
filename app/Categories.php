<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Illuminate\Support\Facades\Auth;

class Categories extends Model
{
    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public static function getProductCategoriesDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = Categories::orderBy('name')->paginate($perPage);
        }
        else
        {
            $data = Categories::where('created_by' ,$user->id)->orderBy('name')->paginate($perPage);
        }

        return $data;
    }
}

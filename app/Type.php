<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Type extends Model
{
    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function categories()
    {
        return $this->hasMany(Categories::class);
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public static function getProductTypeDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = Type::orderBy('name')->paginate($perPage);
        }
        else
        {
            $data = Type::where('created_by' ,$user->id)->orderBy('name')->paginate($perPage);
        }

        return $data;
    }
}

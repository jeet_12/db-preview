<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Table extends Model
{
    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public static function getTableDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = Table::orderBy('name')->paginate($perPage);
        }
        else
        {
            $data = Table::where('created_by' ,$user->id)->orderBy('name')->paginate($perPage);
        }

        return $data;
    }
}

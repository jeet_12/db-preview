<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    public function order_detail()
    {
        return $this->hasMany(Order_detail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function table()
    {
        return $this->belongsTo(Table::class);
    }

    public static function getOrderDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = Order::orderBy('no')->paginate($perPage);
        }
        else
        {
            $data = Order::where('created_by' ,$user->id)->orderBy('no')->paginate($perPage);
        }

        return $data;
    }
}

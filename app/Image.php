<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\File;

class Image extends Model
{
    protected $fillable = ['name', 'type', 'for','created_by','updated_by'];
    public function type()
    {
        return $this->hasMany(Type::class);
    }

    public function categories()
    {
        return $this->hasMany(Categories::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public static function uploadImage($imageFile,$folder_nm = null)
    {
        $isCreated = null;
        $destinationPath = base_path().'\public\assets\img\\'.$folder_nm; // upload path

        if (!File::isDirectory($destinationPath))
        {
            $isCreated = File::makeDirectory($destinationPath);
        }

        $destinationPath = base_path().'\public\assets\img\\'.$folder_nm;  // upload path
        $extension = $imageFile->getClientOriginalExtension(); // getting file extension
        $new_name = time().rand(11111, 99999);
        $fileName = $new_name.'.'.$extension; // renameing image
        $upload_success = $imageFile->move($destinationPath, $fileName); // uploading file

        $upload = "";
        if($upload_success)
        {
            $upload = Image::create([
                'name'             => $new_name,
                'type'           => $extension,
                'for'            => $folder_nm,
                'created_by'        => 1,
                'updated_by'       => 1,
            ]);
        }
        return $upload;
    }

    public static function deleteImage($id)
    {
        $image = Image::findorfail($id);

        $pathToFile = public_path().'\assets\img\\'.$image->for.'\\'.$image->name.'.'.$image->type;
        $status = File::delete($pathToFile);
        $image->delete();
        return $status;
    }
}

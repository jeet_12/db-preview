<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'date' => 'required',
            'time' => 'required',
            'no' => 'required|numeric|min:0',
            'status' => 'required',
            'waiter' => 'required|numeric|min:0',
            'customer' => 'required|numeric|min:0',
            'total' => 'required|numeric|min:0',
        ];

        foreach($this->request->get('product_name') as $key => $value)
        {
            $rules['product_name.'.$key] = 'required';
        }

        foreach($this->request->get('product_price') as $key => $value)
        {
            $rules['product_price.'.$key] = 'required|numeric|min:0';
        }

        foreach($this->request->get('product_quantity') as $key => $value)
        {
            $rules['product_quantity.'.$key] = 'required|numeric|min:0';
        }

        foreach($this->request->get('amount') as $key => $value)
        {
            $rules['amount.'.$key] = 'required|numeric|min:0';
        }


        return $rules;
    }

    public function messages()
    {
        $messages = [];

        foreach($this->request->get('product_name') as $key => $value)
        {
            $messages['product_name.'.$key.'.required'] = 'The product name field is required.';
        }

        foreach($this->request->get('product_price') as $key => $value)
        {
            $key_temp = $key + 1;
            $messages['product_price.'.$key.'.required'] = 'The product price field is required.';
            $messages['product_price.'.$key.'.numeric'] = 'The product price '.$key_temp.' field must be numeric.';
            $messages['product_price.'.$key.'.min'] = 'The product price '.$key_temp.' field must be greater then 0.';
        }

        foreach($this->request->get('product_quantity') as $key => $value)
        {
            $key_temp = $key + 1;
            $messages['product_quantity.'.$key.'required'] = 'The product quantity '.$key_temp.' field is required.';
            $messages['product_quantity.'.$key.'numeric'] = 'The product quantity '.$key_temp.' field must be numeric.';
            $messages['product_quantity.'.$key.'min'] = 'The product quantity '.$key_temp.' field must be greater then 0.';
        }

        foreach($this->request->get('amount') as $key => $value)
        {
            $messages['amount.'.$key.'.required'] = 'The amount field is required.';
            $messages['amount.'.$key.'.numeric'] = 'The amount '.$key_temp.' field must be numeric.';
            $messages['amount.'.$key.'.min'] = 'The amount '.$key_temp.' field must be greater then 0.';
        }

        return $messages;
    }
}

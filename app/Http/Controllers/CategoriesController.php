<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Image;
use App\Type;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Categories::getProductCategoriesDataPaginate();
        $name = "product-categories";
        return  view('admin.categories.index',compact('categories','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = array('' => '-- Select Type --' )  + Type::all()->pluck('name','id')->toArray();
        $name = "product-categories";
        return view('admin.categories.create',compact('type','name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|min:3|max:225',
            'type'  =>'required',
            'image' => 'sometimes|image|mimes:jpeg,jpg,png',
        ],[
            'name.required'		=> 'The Category Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
            'type.required'			=> 'The Product Type field is required.',
        ]);

        $categories = new Categories();
        $categories->name = $request->name;
        $categories->desc = $request->desc ? $request->desc : "";
        $categories->type_id = $request->type;
        $categories->image_id = 0;
        $categories->created_by = Auth::user()->id;
        $categories->updated_by = Auth::user()->id;
        $categories->save();

        if($categories)
        {
            $imageFile = Input::file('image');

            if($imageFile != null) {
                $image = Image::uploadImage($imageFile, 'product-categories');
                if ($image) {
                    $image_id = $image->id;
                    $update = Categories::where('id', $categories->id)->update(array('image_id' => $image_id));
                }
            }
            Session::flash('message', 'Product category successfully added.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('product-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories  = Categories::findorfail($id);
        $name = 'product-categories';
        return view('admin.categories.view', compact('name','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::findorfail($id);
        $type = array('' => '-- Select Type --' )  + Type::all()->pluck('name','id')->toArray();
        $name = "product-categories";
        return view('admin.categories.update',compact('type','categories','name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|min:3|max:225',
            'type'  =>'required',
            'image' => 'sometimes|image|mimes:jpeg,jpg,png',
        ],[
            'name.required'		=> 'The Category Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
            'type.required'			=> 'The Product Type field is required.',
        ]);

        $categories = Categories::findorfail($id);
        $categories->name = $request->name;
        $categories->desc = $request->desc ? $request->desc : "";
        $categories->type_id = $request->type;
        $categories->updated_by = Auth::user()->id;
        $categories->save();

        if($categories)
        {
            $imageFile = Input::file('image');
            $categories_image_id = $categories->image_id;
            if($imageFile != null) {
                if($categories_image_id != 0)
                { Image::deleteImage($categories_image_id); }
                $image = Image::uploadImage($imageFile, 'product-categories');
                if ($image) {
                    $image_id = $image->id;
                    $update = Categories::where('id', $categories->id)->update(array('image_id' => $image_id));
                }
            }
            Session::flash('message', 'Product category successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('product-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $categories = Categories::findOrFail($id);
        $categories_image_id = $categories->image_id;
        if ( $request->ajax() )
        {
            $categories->delete( $request->all() );

            if($categories_image_id != 0)
            {
                Image::deleteImage($categories_image_id);
            }
            return response(['msg' => 'Product Category deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed deleting the product', 'status' => 'failed']);
    }

    public function search(Request $request)
    {
        $name = "product-categories";
        $search = $request->search;
        $categories = Categories::where('name', 'like', ''.$search.'%')->paginate(config('services.PAGINATE'));

        return view('admin.categories.index', compact('categories', 'name'));
    }
}

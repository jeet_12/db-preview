<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::getUserDataPaginate();
        $name = "user";

        return  view('admin.user.index',compact('users','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::all()->pluck('display_name','id')->toArray();
        $status = array('active'=>'Active','inactive'=>'Inactive');
        $name = "user";
        return view('admin.user.create',compact('role','status','name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'phone' => 'required|numeric|digits:10',
            'email' => 'required|string|email|max:255|unique:users',
            'gender' => 'required',
            'password' => 'required|string|min:6',
            'status' => 'required',
            'role' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'gender' => $request->gender,
            'password' => bcrypt($request->password),
            'status' => $request->status,
        ]);
        $user->roles()->attach($request->role);

        if($user)
        {
            Session::flash('message', 'User successfully added.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user  = User::with('roles')->findorfail($id);
        $name = 'user';
        return view('admin.user.view', compact('name','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('roles')->findorfail($id);
        $role = Role::all()->pluck('display_name','id')->toArray();
        $status = array('active'=>'Active','inactive'=>'Inactive');
        $name = "user";


        foreach($user->roles as $role_user)
        {
            $role_id = ucwords($role_user['id']);
        }
        return view('admin.user.update',compact('user','role_id','role','status','name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'phone' => 'required|numeric|min:10',
            'email' => 'required|string|email|max:255',
            'gender' => 'required',
            'status' => 'required',
            'role' => 'required',
        ]);
        $user = User::with('roles')->findorfail($id);
        $update = $user->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'gender' => $request->gender,
            'status' => $request->status,
        ]);

        $user->roles()->sync($request->role);

        if($update)
        {
            Session::flash('message', 'User successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $user = User::findOrFail($id);
        if ( $request->ajax() )
        {
            $user->delete( $request->all() );

            return response(['msg' => 'User deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed deleting the user', 'status' => 'failed']);
    }

    public function search(Request $request)
    {
        $name = "user";
        $search = $request->search;
        $users = User::where('name', 'like', ''.$search.'%')->paginate(config('services.PAGINATE'));

        return view('admin.user.index', compact('users', 'name'));
    }
}
<?php

namespace App\Http\Controllers;

use App\Table;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $tables = Table::getTableDataPaginate();
        $name = "table";
        return  view('admin.table.index',compact('tables','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $name = "table";
        $status = ['active'=>'Active','inactive'=>'Inactive'];
        return view('admin.table.create',compact('name','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'no'  =>'required|numeric|min:1',
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|max:150',
            'status' => 'required',
        ],[
            'no.required'		=> 'The Table No. field is required.',
            'no.numeric'		=> 'The Table No. field must be numeric.',
            'no.min'		    => 'The Table No. field must be grater then 0.',
            'name.required'		=> 'The Table Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
            'status.required'	=> 'The Table Status field is required',
        ]);

        $table= new Table();
        $table->no = $request->no;
        $table->name = $request->name;
        $table->desc = $request->desc ? $request->desc : "";
        $table->status = $request->status;
        $table->created_by = Auth::user()->id;
        $table->updated_by = Auth::user()->id;
        $table->save();

        if($table)
        {
            Session::flash('message', 'Table successfully added.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('table.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $table = Table::findorfail($id);
        $name = 'table';
        return view('admin.table.view', compact('name','table'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $table= Table::findorfail($id);
        $name = "table";
        $status = ['active'=>'Active','inactive'=>'Inactive'];
        return view('admin.table.update',compact('name','table','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'no'  =>'required|numeric|min:1',
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|max:150',
            'status' => 'required',
        ],[
            'no.required'		=> 'The Table No. field is required.',
            'no.numeric'		=> 'The Table No. field must be numeric.',
            'no.min'		    => 'The Table No. field must be grater then 0.',
            'name.required'		=> 'The Table Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
            'status.required'	=> 'The Table Status field is required',
        ]);
        $table = Table::findorfail($id);
        $table->no = $request->no;
        $table->name = $request->name;
        $table->desc = $request->desc ? $request->desc : "";
        $table->status = $request->status;
        $table->updated_by = Auth::user()->id;
        $table->save();

        if($table)
        {
            Session::flash('message', 'Table successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('table.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $table = Table::findOrFail($id);
        if ( $request->ajax() )
        {
            $table->delete( $request->all() );

            return response(['msg' => 'Table deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed deleting the table', 'status' => 'failed']);
    }

    public function search(Request $request)
    {
        $name = "table";
        $search = $request->search;
        $tables = Table::where('name', 'like', ''.$search.'%')->paginate(config('services.PAGINATE'));

        return view('admin.table.index', compact('tables', 'name'));
    }
}

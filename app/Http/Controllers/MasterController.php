<?php

namespace App\Http\Controllers;

use App\Order;
use App\Table;
use Carbon\Carbon;
use Response;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function table()
    {
        $name = 'table';
        return view('admin.master.table',compact('name'));
    }

    public function order()
    {
        $name = 'order';
        return view('admin.master.order',compact('name'));
    }

    public function TableAjex()
    {
        $data = array();
        $item = array();

        $table = Table::all();

        if(count($table) > 0) {
            $j=1;
            foreach ($table as $tb) {

                $temp = array() ;
                $temp['id'] = $j;
                $temp['color'] = $tb->status == 'active' ? 'green' : 'pink';
                $temp['table_name'] = $tb->name;
                $temp['table_id'] = $tb->id;
                $temp['table_status'] = $tb->status;

                array_push($item,$temp);
                $j++;
            }
            $data['table'] = $item;
        }
        else {
            $data['msg'] = 'error';
        }
        return Response::json($data);
    }


    public function OrderAjex()
    {
        $data = array();
        $item = array();

        $date = Carbon::now()->toDateString();
        $order = Table::join('orders','tables.id','orders.table_id')
            ->join('customers','orders.customer_id','customers.id')
            ->select('customers.name AS cust_name','tables.name AS tab_name','tables.id AS tab_no','orders.time AS ord_time','orders.id AS ord_id','orders.status AS ord_status')
            ->where('orders.date',$date)
            ->where('orders.status','in-serve')
//            ->orwhere('orders.status','pending')
            ->where('tables.status','active')->get();
//        dd($order);
        if(count($order) > 0) {
            $j=1;
            foreach ($order as $od) {

                $temp = array() ;
                $temp['id'] = $j;
                $temp['color'] = $od->ord_status == 'in-serve' ? 'green' : 'pink';
                $temp['customer_name'] = $od->cust_name;
                $temp['table_id'] = $od->tab_no;
                $temp['order_status'] = $od->ord_status;
                $temp['order_id'] = $od->ord_id;
                array_push($item,$temp);
                $j++;
            }
            $data['table'] = $item;
        }
        else {
            $data['msg'] = 'error';
        }

        return Response::json($data);
    }

    public function cookOrder()
    {
        $name = 'Order';
        return view('cook.order',compact('name'));
    }

    public function cookOrderAjex()
    {
        $data = array();
        $item = array();
        $date = Carbon::now()->toDateString();
        $table = Table::join('orders','tables.id','orders.table_id')
            ->join('customers','orders.customer_id','customers.id')
            ->select('customers.name AS cust_name','tables.name AS tab_name','tables.id AS tab_no','orders.time AS ord_time')
            ->where('orders.date',$date)
            ->where('tables.status','active')->get();
//        dd($table);
        if(count($table) > 0) {
            $j=1;
            foreach ($table as $tb) {

                $temp = array() ;
                $temp['id'] = $tb->tab_no;
                $temp['color'] = 'green';
                $temp['cust_name'] = $tb->cust_name;
                $temp['order_time'] = $tb->ord_time;

                array_push($item,$temp);
                $j++;
            }
            $data['table'] = $item;
        }
        else {
            $data['msg'] = 'error';
        }
//                dd($data);
        return Response::json($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Image;
use App\Product;
use App\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::getProductDataPaginate();
        $name = "product";
        return  view('admin.product.index',compact('products','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = array('' => '-- Select Type --' )  + Type::all()->pluck('name','id')->toArray();
        $categories = array('' => '-- Select Categories --' ) + Categories::all()->pluck('name','id')->toArray();
        $name = "product";
        return view('admin.product.create',compact('type','categories','name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|min:3|max:150',
            'price'  =>'required|numeric',
            'type'  =>'required',
            'categories'  =>'required',
            'image' => 'sometimes|image|mimes:jpeg,jpg,png',
        ],[
            'name.required'		=> 'The Product Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
            'price.required'	=> 'The Price field is required.',
            'type.required'	=> 'The Type field is required.',
            'categories.required'	=> 'The Category field is required.',
        ]);
        $product = new Product();
        $product->name = $request->name;
        $product->desc = $request->desc ? $request->desc : "";
        $product->price = $request->price;
        $product->type_id = $request->type;
        $product->categories_id = $request->categories;
        $product->image_id = 0;
        $product->created_by = Auth::user()->id;
        $product->updated_by = Auth::user()->id;
        $product->save();

        if($product)
        {
            $imageFile = Input::file('image');

            if($imageFile != null) {
                $image = Image::uploadImage($imageFile, 'product');
                if ($image) {
                    $image_id = $image->id;
                    $update = Product::where('id', $product->id)->update(array('image_id' => $image_id));
                }
            }
            Session::flash('message', 'Product successfully added.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product  = Product::findorfail($id);
        $name = 'product';
        return view('admin.product.view', compact('name','product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $type = array('' => '-- Select Type --' )  + Type::all()->pluck('name','id')->toArray();
        $categories = array('' => '-- Select Categories --' ) + Categories::all()->pluck('name','id')->toArray();
        $name = "product";

        return view('admin.product.update',compact('type','categories','name','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|min:3|max:150',
            'price'  =>'required|numeric',
            'type'  =>'required',
            'categories'  =>'required',
            'image' => 'sometimes|image|mimes:jpeg,jpg,png',
        ],[
            'name.required'		=> 'The Product Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
            'price.required'	=> 'The Price field is required.',
            'type.required'	=> 'The Type field is required.',
            'categories.required'	=> 'The Category field is required.',
        ]);

        $product = Product::findorfail($id);
        $product->name = $request->name;
        $product->desc = $request->desc ? $request->desc : "";
        $product->price = $request->price;
        $product->type_id = $request->type;
        $product->categories_id = $request->categories;
        $product->updated_by = Auth::user()->id;
        $product->save();

        if($product)
        {
            $imageFile = Input::file('image');
            $product_image_id = $product->image_id;
            if($imageFile != null) {
                if($product_image_id != 0)
                { Image::deleteImage($product_image_id); }
                $image = Image::uploadImage($imageFile, 'product');
                if ($image) {
                    $image_id = $image->id;
                    $update = Product::where('id', $product->id)->update(array('image_id' => $image_id));
                }
            }
            Session::flash('message', 'Product successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $product = Product::findOrFail($id);
        $product_image_id = $product->image_id;
        if ( $request->ajax() )
        {
            $product->delete( $request->all() );

            if($product_image_id != 0)
            {
                Image::deleteImage($product_image_id);
            }
            return response(['msg' => 'Product deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed deleting the product', 'status' => 'failed']);
    }

    public function search(Request $request)
    {
        $name = "product";
        $search = $request->search;
        $products = Product::where('name', 'like', ''.$search.'%')->paginate(config('services.PAGINATE'));

        return view('admin.product.index', compact('products', 'name'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Image;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Session;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $types = Type::getProductTypeDataPaginate();
        $name = "product-type";
        return  view('admin.type.index',compact('types','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $name = "product-type";
        return view('admin.type.create',compact('name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|max:150',
            'image' => 'sometimes|image|mimes:jpeg,jpg,png',
        ],[
            'name.required'		=> 'The Type Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
        ]);

        $type= new Type();
        $type->name = $request->name;
        $type->desc = $request->desc ? $request->desc : "";
        $type->image_id = 0;
        $type->created_by = Auth::user()->id;
        $type->updated_by = Auth::user()->id;
        $type->save();

        if($type)
        {
            $imageFile = Input::file('image');

            if($imageFile != null) {
                $image = Image::uploadImage($imageFile, 'product-type');
                if ($image) {
                    $image_id = $image->id;
                    $update = Type::where('id', $type->id)->update(array('image_id' => $image_id));
                }
            }
            Session::flash('message', 'Product type successfully added.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('product-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $types  = Type::findorfail($id);
        $name = 'product-type';
        return view('admin.type.view', compact('name','types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types = Type::findorfail($id);
        $name = "product-type";
        return view('admin.type.update',compact('name','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'  =>'required|min:3|max:150',
            'desc'  =>'sometimes|min:3|max:150',
            'image' => 'sometimes|image|mimes:jpeg,jpg,png',
        ],[
            'name.required'		=> 'The Type Name field is required.',
            'name.min'			=> 'The Name field must be 3 to 225 character',
            'name.max'			=> 'The Name field must be 3 to 225 character',
            'desc.min'			=> 'The Description field must be 3 to 225 character',
            'desc.max'			=> 'The Description field must be 3 to 225 character',
        ]);

        $type = Type::findorfail($id);
        $type->name = $request->name;
        $type->desc = $request->desc ? $request->desc : "";
        $type->updated_by = Auth::user()->id;
        $type->save();

        if($type)
        {
            $imageFile = Input::file('image');
            $type_image_id = $type->image_id;
            if($imageFile != null) {
                if($type_image_id != 0)
                { Image::deleteImage($type_image_id); }
                $image = Image::uploadImage($imageFile, 'product-type');
                if ($image) {
                    $image_id = $image->id;
                    $update = Type::where('id', $type->id)->update(array('image_id' => $image_id));
                }
            }
            Session::flash('message', 'Product type successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('product-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $type = Type::findOrFail($id);
        $type_image_id = $type->image_id;
        if ( $request->ajax() )
        {
            $type->delete( $request->all() );

            if($type_image_id != 0)
            {
                Image::deleteImage($type_image_id);
            }
            return response(['msg' => 'Product type deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed deleting the product type', 'status' => 'failed']);
    }

    public function search(Request $request)
    {
        $name = "product-type";
        $search = $request->search;
        $types = Type::where('name', 'like', ''.$search.'%')->paginate(config('services.PAGINATE'));

        return view('admin.type.index', compact('types', 'name'));
    }
}

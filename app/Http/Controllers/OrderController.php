<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\OrderRequest as OrderRequest;
use App\Order;
use App\Order_detail;
use App\Product;
use App\User;
use Response;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::getOrderDataPaginate();
        $name = "order";
        return  view('admin.order.index',compact('orders','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $order_no = $this->getOrderNo();
        $user = User::where('status','active')->pluck('name','id');
        $customer = Customer::pluck('name','id');
        $product = Product::pluck('name','id');
        $products = Product::select('id','name')->get();
        $name = "order";
        $status = ['in-serve'=>'In-serve','complete'=>'Complete','pending'=>'Pending','cancel'=>'Cancel'];
        return view('admin.order.create',compact('name','product','products','customer','user','status','order_no'));
    }

    public function getOrderNo(){
        do{
            $rand = $this->generateRandomString(6);
        }while(!empty(Order::where('id',$rand)->first()));
        return '#DB'.$rand;
    }
    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $order = new Order();
        $order->customer_id = $request->customer;
        $order->table_id = $request->no;
        $order->user_id = $request->waiter;
        $order->no = $request->order_no;
        $order->date = date('Y-m-d', strtotime($request->date));
        $order->time = date("H:i", strtotime($request->time));
        $order->status = $request->status;
        $order->created_by = Auth::user()->id;
        $order->updated_by = Auth::user()->id;
        $order->save();

        $count = count($request->product_name);
        if($count > 0) {
            for($i=0;$i<$count;$i++)
            {
                $product = Product::findorfail($request->product_name[$i]);
                $order_detail = new Order_detail();
                $order_detail->order_id = $order->id;
                $order_detail->product_id = $product->id;
                $order_detail->product_name = $product->name;
                $order_detail->product_price = $request->product_price[$i];
                $order_detail->qty = $request->product_quantity[$i];
                $order_detail->amount = $request->amount[$i];
                $order_detail->created_by = Auth::user()->id;
                $order_detail->updated_by = Auth::user()->id;
                $order_detail->save();
            }
        }
        if($order)
        {
            Session::flash('message', 'Order successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('order.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order  = Order::findorfail($id);
        $name = 'order';
        return view('admin.order.view', compact('name','order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findorfail($id);
        $user = User::where('status','active')->pluck('name','id');
        $customer = Customer::pluck('name','id');
        $product = Product::pluck('name','id');
        $products = Product::select('id','name')->get();
        $name = "order";
        $status = ['in-serve'=>'In-serve','complete'=>'Complete','pending'=>'Pending','cancel'=>'Cancel'];
        return view('admin.order.update',compact('name','product','products','customer','user','status','order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        $order = Order::findorfail($id);
        $order->customer_id = $request->customer;
        $order->table_id = $request->no;
        $order->user_id = $request->waiter;
        $order->date = date('Y-m-d', strtotime($request->date));
        $order->time = date("H:i", strtotime($request->time));
        $order->status = $request->status;
        $order->updated_by = Auth::user()->id;
        $order->save();

        $count = count($request->product_name);

        if($count > 0) {
            Order_detail::where('order_id',$id)->delete();
            for($i=0;$i<$count;$i++)
            {
                $product = Product::findorfail($request->product_name[$i]);
                $order_detail = new Order_detail();
                $order_detail->order_id = $id;
                $order_detail->product_id = $product->id;
                $order_detail->product_name = $product->name;
                $order_detail->product_price = $request->product_price[$i];
                $order_detail->qty = $request->product_quantity[$i];
                $order_detail->amount = $request->amount[$i];
                $order_detail->created_by = Auth::user()->id;
                $order_detail->updated_by = Auth::user()->id;
                $order_detail->save();
            }
        }
        if($order)
        {
            Session::flash('message', 'Order successfully updated.');
            Session::flash('alert-class', 'alert-success');
        }
        else
        {
            Session::flash('message', 'Error! Something went wrong.');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect()->route('order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $order = Order::findOrFail($id);
        if ( $request->ajax() )
        {
            $order->delete( $request->all() );

            return response(['msg' => 'Product deleted', 'status' => 'success']);
        }
        return response(['msg' => 'Failed deleting the product', 'status' => 'failed']);
    }

    public function search(Request $request)
    {
        $name = "order";
        $search = $request->search;
        $orders = Order::where('name', 'like', ''.$search.'%')->paginate(config('services.PAGINATE'));

        return view('admin.order.index', compact('orders', 'name'));
    }

    public function getPrice(Request $request)
    {
        $id = $request->id;
        $product_price = Product::where('id',$id)->value("price");

        return Response::json($product_price);
    }
}

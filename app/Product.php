<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    public function categories()
    {
        return $this->belongsTo(Categories::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public static function getProductDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = Product::orderBy('name')->paginate($perPage);
        }
        else
        {
            $data = Product::where('created_by' ,$user->id)->orderBy('name')->paginate($perPage);
        }

        return $data;
    }
}
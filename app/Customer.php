<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public static function getCustomerDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = Customer::orderBy('name')->paginate($perPage);
        }
        else
        {
            $data = Customer::where('created_by' ,$user->id)->orderBy('name')->paginate($perPage);
        }

        return $data;
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','phone', 'email','gender','password','status','created_by','updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function role()
    {
        return $this->hasMany(role::class);
    }

    public static function getUserDataPaginate()
    {
        $perPage = config('services.PAGINATE');
        $user = Auth::user();
        if($user->hasRole('admin'))
        {
            $data = User::with('roles')->orderBy('name')->paginate($perPage);
        }
        else
        {
            $data = User::where('created_by' ,$user->id)->orderBy('name')->roles->paginate($perPage);
        }

        return $data;
    }
}

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'SITE_DETAILS' => [
        'SITE_TITLE'                    => 'DineBuddy',
        'SITE_NAME'                     => 'DineBuddy',
        'SITE_DEVELOPER'                => 'Harjot Saini',
        'SITE_CREATE_COMPANY'           => '',
        'SITE_CREATE_COMPANY_WEBSITE'   => '',
        'SITE_ADMIN_MOBILE'             => '',
    ],

    'SOCIAL' => [
        'FACEBOOK'          => 'https://www.facebook.com/',
        'INSTAGRAM'         => 'https://www.instagram.com/',
        'TWITTER'           => 'https://twitter.com/',
        'YOUTUBE'           => 'https://www.youtube.com/',
        'PINTEREST'         => 'https://www.pinterest.com/',
    ],

    'PAGINATE' => '10',

];

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/', function () {
//
//    return view('admin.dashboard');
//});


Route::get('/', 'HomeController@index')->name('home');
Auth::routes();


Route::group(['prefix' => 'admin', 'middleware' => ['role:admin|cashier']], function () {

    Route::get('/table/ajax','MasterController@TableAjex');
    Route::get('/order/ajax','MasterController@OrderAjex');

    Route::get('/dashboard', 'HomeController@Dashboard')->name('admin.dashboard');;

    Route::post('/user-search','UserController@search');
    Route::resource('user','UserController');

    Route::post('/product-search','ProductController@search');
    Route::resource('product','ProductController');

    Route::post('/product-type-search','TypeController@search');
    Route::resource('product-type','TypeController');

    Route::post('/product-categories-search','CategoriesController@search');
    Route::resource('product-categories','CategoriesController');

    Route::post('/table-search','TableController@search');
    Route::resource('table','TableController');

    Route::post('/order-search','OrderController@search');
    Route::resource('order','OrderController');

    Route::get('/table-grid','MasterController@table');
    Route::get('/order-grid','MasterController@order');

    Route::get('/getprice','OrderController@getPrice');

});

Route::group(['prefix' => 'cook', 'middleware' => ['role:admin|cook']], function () {
    Route::get('/order/table/ajax','MasterController@cookOrderAjex');
    Route::get('/order','MasterController@cookOrder');
});




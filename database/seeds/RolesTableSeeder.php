<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Administrator',
            'description'  => 'System Administrator',
        ]);

        DB::table('roles')->insert([
            'name' => 'cashier',
            'display_name' => 'Cashier',
            'description'  => 'Restaurant Cashier',
        ]);

        DB::table('roles')->insert([
            'name' => 'cook',
            'display_name' => 'Restaurant Cook',
            'description'  => 'Restaurant Cook',
        ]);

//        DB::table('roles')->insert([
//            'name' => 'customer',
//            'display_name' => 'Restaurant Customer',
//            'description'  => 'Restaurant Customer',
//        ]);
    }
}

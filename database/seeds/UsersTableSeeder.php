t<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Administrator",
            'email' => 'support@dinebuddy.live',
            'phone' => '0',
            'gender' => 'male',
            'password' => bcrypt('secret'),
            'status' => "active",
        ]);

        DB::table('users')->insert([
            'name' => "Harjot",
            'email' => 'harjotsingh.saini@yahoo.com',
            'phone' => '0',
            'gender' => 'male',
            'password' => bcrypt('secret'),
            'status' => "active",
        ]);

        DB::table('users')->insert([
            'name' => "Jeet",
            'email' => 'jeetys@outlook.com',
            'password' => bcrypt('secret'),
            'phone' => '0000000000',
            'gender' => 'male',
            'status' => "active",
        ]);

        DB::table('users')->insert([
            'name' => "Cook",
            'email' => 'cook@test.com',
            'password' => bcrypt('secret'),
            'phone' => '0000000000',
            'gender' => 'male',
            'status' => "active",
        ]);

    }
}

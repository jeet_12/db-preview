<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(PermissionTableSeeder::class);
         $this->call(Role_UsersTableSeeder::class);
        factory(App\Type::class,10)->create();
        factory(App\Categories::class,20)->create();
        factory(App\Product::class,30)->create();
        factory(App\Table::class,10)->create();
        factory(App\Customer::class,30)->create();
        factory(App\Order::class,30)->create();
        factory(App\Order_detail::class,60)->create();
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name'          => 'user-add',
            'display_name'  => 'Add New User',
            'description'   => 'Add New User',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'user-view',
            'display_name'  => 'View User',
            'description'   => 'View User',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'user-update',
            'display_name'  => 'Edit User',
            'description'   => 'Edit User',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'user-delete',
            'display_name'  => 'Delete User',
            'description'   => 'Delete User',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'user-status',
            'display_name'  => 'Change User Status',
            'description'   => 'Change User Status',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'type-add',
            'display_name'  => 'Add New Type',
            'description'   => 'Add New Type',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'type-view',
            'display_name'  => 'View Type',
            'description'   => 'View Type',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'type-edit',
            'display_name'  => 'Edit Type',
            'description'   => 'Edit Type',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'type-delete',
            'display_name'  => 'Delete Type',
            'description'   => 'Delete Type',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'category-add',
            'display_name'  => 'Add New Category',
            'description'   => 'Add New Category',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'category-view',
            'display_name'  => 'View Category',
            'description'   => 'View Category',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'category-edit',
            'display_name'  => 'Add New Category',
            'description'   => 'Add New Category',
        ]);

        DB::table('permissions')->insert([
            'name'          => 'category-delete',
            'display_name'  => 'Delete Category',
            'description'   => 'Delete Category',
        ]);

    }
}

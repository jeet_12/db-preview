<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Type::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->word,
        'desc' => $faker->sentence(),
        'image_id' => 0,
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Categories::class, function (Faker\Generator $faker) {

    return [
        'type_id' => App\Type::all()->random()->id,
        'name' => $faker->word,
        'desc' => $faker->sentence(),
        'image_id' => 0,
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {

    return [
        'type_id' => App\Type::all()->random()->id,
        'categories_id' => App\Categories::all()->random()->id,
        'image_id' => 0,
        'name' => $faker->word,
        'desc' => $faker->sentence(),
        'price' => $faker->randomDigit,
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Table::class, function (Faker\Generator $faker) {

    return [
        'no' => $faker->randomDigit,
        'name' => $faker->word,
        'desc' => $faker->sentence(),
        'status' => $faker->randomElement(['active','inactive']),
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->randomNumber(),
        'address' => $faker->address,
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Order::class, function (Faker\Generator $faker) {

    return [
        'customer_id' => App\Customer::all()->random()->id,
        'table_id' => App\Table::all()->random()->id,
        'user_id' => App\User::all()->random()->id,
        'no' => $faker->unique()->word,
        'date' => $faker->date('Y-m-d',$startDate = "now", $endDate = "30 days"),
        'time' => $faker->time('H:i:s'),
        'status' => $faker->randomElement(['in-serve','complete','pending','cancel']),
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});

$factory->define(App\Order_detail::class, function (Faker\Generator $faker) {
    return [
        'order_id' => App\Order::all()->random()->id,
        'product_id' => App\Product::all()->random()->id,
        'product_name' => $faker->word,
        'product_price' => $faker->randomDigit,
        'qty' => $faker->randomDigit,
        'created_by' => App\User::all()->random()->id,
        'updated_by' => App\User::all()->random()->id,
    ];
});
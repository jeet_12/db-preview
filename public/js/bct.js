var baseUrl     = $('meta[name="_url"]').attr('content');
var token       = $('meta[name="csrf-token"]').attr('content');
var fa_spin     = '<i class="fa fa-spinner fa-spin  fa-fw text-danger"></i>';
var fa_trash    = '<i class="fa fa-trash-o"></i>';

$(document).tooltip({
    selector: '[data-toggle="tooltip"]',
    placement: 'top',
    trigger: "hover"
});

// $( document ).ready(function() {
//     notificationCount();
//     setInterval(function(){ notificationCount() }, 20000);
// });


$(document).on( "click", ".table-col-del", function() {

    var r = confirm("Are you sure delete this record ?");
    if (r == true)
    {
        var id = $(this).attr('data-id');
        var p_id = $(this).attr('data-pid');
        var _token =  token;
        var _url =  baseUrl;
        var form_data = {_method: 'delete', _token : _token, id:id};
        var url = _url+'/admin/'+p_id+'/'+id;
        $(this).html(fa_spin);

        $.ajax({
            url: url,
            type: 'POST',
            data: form_data,
            dataType:"json",
            success: function(result)
            {
                var temp_string = '.'+p_id+'_table_row'+id;
                if(result.status === 'success')
                {
                    updateIntimation(temp_string,'del');
                }
                else
                {
                    updateIntimation(temp_string,'err');
                    $(this).html(fa_trash);
                }
            }
        });
    }
});


$(".table-col-sta").click(function (){
    var r = confirm("Are you sure Change this Status ?");
    if (r == true)
    {

        var id = $(this).attr('data-id');
        var p_id = $(this).attr('data-pid');
        var _token =  token;
        var _url =  baseUrl;
        var form_data = { id:id,_token:_token};
        var url = _url+'/admin/'+p_id+'/status';
        var update_rec = this;
        $(update_rec).html('<i class="fa fa-spinner fa-spin  fa-fw text-success"></i>');


        $.ajax({
            url: url,
            type: 'POST',
            data: form_data,
            dataType:"json",
            success: function(result)
            {
                if(result.status == 'active')
                {
                    $(update_rec).html('<i class="fa fa-dot-circle-o text-success"></i>');
                    $(".row-col-sta"+id).html('<span class="label label-success" data-placement="top" data-toggle="tooltip" title="Active">Active</span>');
                }
                else
                {
                    $(update_rec).html('<i class="fa fa-dot-circle-o text-danger"></i>');
                    $(".row-col-sta"+id).html('<span class="label label-default" data-placement="top" data-toggle="tooltip" title="Inactive">Inactive</span>');
                }
                updateIntimation(".row-col-sta"+id);
            }
        });
    }
});




function updateIntimation(attribute,status)
{
    if(status == 'del')
    {
        $(attribute).addClass("selected");
        setTimeout(function() { $(attribute).remove(); }, 500);
    }
    if(status == 'err')
    {
        $(attribute).addClass("selected");
        setTimeout(function() { $(attribute).remove(); }, 500);
    }
    else
    {
        $(attribute).addClass("selected");
        setTimeout(function() { $(attribute).removeClass("selected"); }, 6000);
    }
}



